package apperrors

import "fmt"

// AppError //
type AppError struct {
	Code    int64
	Message string
}

func (ae AppError) Error() string {
	return fmt.Sprintf("AE[%d]: %s", ae.Code, ae.Message)
}

// New //
func New(code int64, message string) *AppError {
	return &AppError{
		Code:    code,
		Message: message,
	}
}

// Error codes
const (
	EntityExists = iota
)

// Error variables //
var (
	Exists = New(EntityExists, "entity already exists")
)
