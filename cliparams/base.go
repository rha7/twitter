package cliparams

import "flag"

// CliParams //
type CliParams struct {
	Host   string
	Port   int64
	DBPath string
}

// ProcessCliParams //
func ProcessCliParams(args []string) (*CliParams, error) {
	cliParams := &CliParams{}
	fs := flag.NewFlagSet(args[0], flag.ContinueOnError)
	helpHelp := fs.Bool("help", false, "print usage")
	helpH := fs.Bool("h", false, "print usage")
	fs.StringVar(&cliParams.Host, "host", "0.0.0.0", "host name or address")
	fs.Int64Var(&cliParams.Port, "port", 8693, "port number")
	fs.StringVar(&cliParams.DBPath, "dbpath", "./twitter.sqlite3", "sqlite3 db path")
	fs.Parse(args[1:])
	if *helpHelp || *helpH {
		fs.PrintDefaults()
		return nil, flag.ErrHelp
	}
	return cliParams, nil
}
