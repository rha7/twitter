package cliparams_test

import (
	"flag"
	"testing"

	"bitbucket.org/rha7/twitter/cliparams"

	. "github.com/smartystreets/goconvey/convey"
)

func TestProcessCliParams(t *testing.T) {
	Convey("Defaults", t, func() {
		_, err := cliparams.ProcessCliParams([]string{
			"appname",
		})
		So(err, ShouldBeNil)
	})
	Convey("Params passes", t, func() {
		cliParams, err := cliparams.ProcessCliParams([]string{
			"appname",
			"-host",
			"1.2.3.4",
			"-port",
			"4321",
			"-dbpath",
			"./somedb.sqlite3",
		})
		So(err, ShouldBeNil)
		So(cliParams.Host, ShouldEqual, "1.2.3.4")
		So(cliParams.Port, ShouldEqual, 4321)
		So(cliParams.DBPath, ShouldEqual, "./somedb.sqlite3")
	})
	Convey("Help", t, func() {
		_, err := cliparams.ProcessCliParams([]string{
			"appname",
			"-h",
		})
		So(err, ShouldEqual, flag.ErrHelp)
	})
}
