package config

import (
	"fmt"

	"bitbucket.org/rha7/twitter/cliparams"
)

// AppConfig //
type AppConfig struct {
	Host   string
	Port   int64
	DBPath string
}

// BindAddress //
func (ac AppConfig) BindAddress() string {
	return fmt.Sprintf("%s:%d", ac.Host, ac.Port)
}

// NewAppConfig //
func NewAppConfig(cliParams *cliparams.CliParams) *AppConfig {
	return &AppConfig{
		Host:   cliParams.Host,
		Port:   cliParams.Port,
		DBPath: cliParams.DBPath,
	}
}
