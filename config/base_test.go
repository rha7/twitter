package config_test

import (
	"testing"

	"bitbucket.org/rha7/twitter/cliparams"
	"bitbucket.org/rha7/twitter/config"

	. "github.com/smartystreets/goconvey/convey"
)

func TestConfig(t *testing.T) {
	Convey("NewAppConfig", t, func() {
		ac := config.NewAppConfig(&cliparams.CliParams{
			Host:   "4.4.4.4",
			Port:   4444,
			DBPath: "./somedb.sqlite3",
		})
		So(ac, ShouldNotBeNil)
		So(ac.Host, ShouldEqual, "4.4.4.4")
		So(ac.Port, ShouldEqual, 4444)
		So(ac.DBPath, ShouldEqual, "./somedb.sqlite3")
		So(ac.BindAddress(), ShouldEqual, "4.4.4.4:4444")
	})
}
