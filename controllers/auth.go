package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/rha7/twitter/dal"
	"bitbucket.org/rha7/twitter/responses"
)

// Auth //
type Auth struct {
	DALManager dal.Manager
}

// NewAuth //
func NewAuth(dalMgr dal.Manager) *Auth {
	return &Auth{
		DALManager: dalMgr,
	}
}

// GetToken //
func (auth *Auth) GetToken(w http.ResponseWriter, r *http.Request) {
	type InputParams struct {
		UserName string `json:"username,omitempty" validate:"required"`
		Password string `json:"password,omitempty" validate:"required"`
	}
	inputParams := &InputParams{}
	jsonDecoder := json.NewDecoder(r)
	err := jsonDecoder.Decode(inputParams)
	if err != nil {
		responses.Generic(w, http.StatusBadRequest, map[string]string{
			"error": "invalid reques",
		})
	}
	err := auth.DALManager.IsValidUser(inputParams.UserName, inputParams.Password)
	if err != nil {
		responses.Generic(w, http.StatusUnauthorized, map[string]string{
			"error": "unauthorized user",
		})
		return
	}
	// TODO: generate JWT token here
}
