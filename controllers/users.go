package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/rha7/twitter/apperrors"
	"bitbucket.org/rha7/twitter/config"
	"bitbucket.org/rha7/twitter/dal"
	"bitbucket.org/rha7/twitter/responses"
)

// Users //
type Users struct {
	DALManager dal.Manager
}

// NewUsers //
func NewUsers(appConfig *config.AppConfig, dalmgr dal.Manager) *Users {
	return &Users{
		DALManager: dalmgr,
	}
}

// New //
func (uc Users) New(w http.ResponseWriter, r *http.Request) {
	type InputParams struct {
		UserName string `json:"username,omitempty" validate:"required"`
		Password string `json:"password,omitempty" validate:"required"`
	}
	inputParams := &InputParams{}
	jsonDecoder := json.NewDecoder(r.Body)
	err := jsonDecoder.Decode(inputParams)
	if err != nil {
		responses.Generic(w, http.StatusBadRequest, map[string]string{"error": "invalid request format (is it json?)"})
		return
	}
	err = validate.Struct(inputParams)
	if err != nil {
		responses.Generic(w, http.StatusUnprocessableEntity, map[string]string{
			"error": "invalid parameters",
		})
		return
	}
	id, err := uc.DALManager.NewUser(inputParams.UserName, inputParams.Password)
	if err != nil {
		switch true {
		case err == apperrors.Exists:
			responses.Generic(w, http.StatusUnprocessableEntity, map[string]string{
				"error": err.Error(),
			})
		default:
			responses.Generic(w, http.StatusUnprocessableEntity, map[string]string{
				"error": fmt.Sprintf("unexpected error: %s", err.Error()),
			})
		}
		return
	}
	responses.Generic(w, http.StatusOK, map[string]interface{}{
		"status":   "ok",
		"id":       id,
		"username": inputParams.UserName,
		"password": inputParams.Password,
	})
}
