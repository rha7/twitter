package dal

// Manager //
type Manager interface {
	Init() error
	NewUser(string, string) (int64, error)
}
