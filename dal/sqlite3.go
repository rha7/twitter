package dal

import (
	"database/sql"
	"fmt"
	"os"

	"bitbucket.org/rha7/twitter/config"

	// Blank for adapter package
	_ "github.com/mattn/go-sqlite3"
)

// Sqlite3 //
type Sqlite3 struct {
	DBPath string
	DB     *sql.DB
}

var _ Manager = &Sqlite3{}

// NewSqlite3 //
func NewSqlite3(appConfig *config.AppConfig) *Sqlite3 {
	return &Sqlite3{
		DBPath: appConfig.DBPath,
		DB:     nil,
	}
}

// Init //
func (dal *Sqlite3) Init() error {
	_, err := os.Stat(dal.DBPath)
	if err != nil {
		panic(err)
	}
	db, err := sql.Open("sqlite3", dal.DBPath)
	if err != nil {
		return err
	}
	dal.DB = db
	return nil
}

// NewUser //
func (dal *Sqlite3) NewUser(username string, password string) (int64, error) {
	fmt.Sprintf("%s:%s\n", username, password)
	res, err := dal.DB.Exec("INSERT INTO users (username, password) values (?, ?)", username, password)
	if err != nil {
		return -1, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return -1, err
	}
	return id, nil
}
