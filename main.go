package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/rha7/twitter/cliparams"
	"bitbucket.org/rha7/twitter/config"
	"bitbucket.org/rha7/twitter/controllers"
	"bitbucket.org/rha7/twitter/dal"
	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func main() {
	cliParams, err := cliparams.ProcessCliParams(os.Args)
	if err != nil {
		if err == flag.ErrHelp {
			os.Exit(255)
		}
		panic(err)
	}
	appConfig := config.NewAppConfig(cliParams)

	fmt.Printf("%#v\n", appConfig)

	dalManager := dal.NewSqlite3(appConfig)
	err = dalManager.Init()
	if err != nil {
		panic(err)
	}

	logrus.
		WithField("bindAddress", appConfig.BindAddress()).
		Info("params")

	usersController := controllers.NewUsers(appConfig, dalManager)

	r := mux.NewRouter()

	r.HandleFunc("/users", usersController.New).Methods("POST")

	n := negroni.Classic()

	n.UseHandler(r)

	logrus.
		WithField("bind_address", appConfig.BindAddress()).
		Info("starting")
	err = http.ListenAndServe(appConfig.BindAddress(), n)

	if err != nil {
		panic(err)
	}
}
