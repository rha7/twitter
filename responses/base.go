package responses

import (
	"encoding/json"
	"net/http"
)

// GenericResponse //
func Generic(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	jsonEncoder := json.NewEncoder(w)
	jsonEncoder.Encode(payload)
}
