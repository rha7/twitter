CREATE TABLE users (id integer primary key autoincrement, username varchar(64), password varchar(64));
CREATE UNIQUE INDEX users_usename on users(username);
